<?php


namespace LiLei\Weather;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Weather::class, function(){
            return new Weather(config('services.weather.key'));
        });

        $this->app->alias(Weather::class, 'weather');
    }//register() end

    public function provides()
    {
        return [Weather::class, 'weather'];
    }//provides() end
}